<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class
status extends Model
{
    const LINE1 = "Eerstelijn";
    //The A in LINE1A means "Assigned"
    const LINE1A = "Eerstelijn_toegewezen";
    const LINE2 = "Tweedelijn";
    const LINE2A = "Tweedelijn_toegewezen";
    const HANDLED = "Afgehandeld";
    
    
    function tickets(){
        return $this->hasMany("App\Ticket");
    }
}
