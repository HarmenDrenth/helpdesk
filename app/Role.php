<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Role extends Authenticatable
{
    const ADMIN = "admin";
    const KLANT = "klant";
    const LINE1 = "line1";
    const LINE2 = "line2";
    
    public function users(){
        return $this->hasMany("App\User");
    }
}