<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\status;

class Ticket extends Model
{
    function customer_user(){
        return $this->belongsTo("App\User","user_id");
    }
    
    function status(){
        return $this->belongsTo("App\Status");
    }
    
    function helpdesk_users(){
        return $this->belongsToMany("App\User");
    }
    
    function comments(){
        return $this->hasMany("App\Comment");
    }
    
    function isOpen(){
        return $this->status->name != status::HANDLED;
    }
}
