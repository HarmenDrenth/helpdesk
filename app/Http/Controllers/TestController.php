<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function show($id)
    {
        return view('testparameter', ['id' => $id]);
    }
    
    public function showWelc(){
        return view("welcome");
    }
}
