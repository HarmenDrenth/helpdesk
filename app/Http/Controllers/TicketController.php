<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Status;
use App\Ticket;
use App\Role;

class TicketController extends Controller
{
    
    
    public function __construct(){
        $this->middleware("auth");
    }
    
    public function create(){
        $this->authorize("create", Ticket::class);
        return view("ticket.create");
    }
    
    public function save(Request $request){
        $this->authorize("create", Ticket::class);
        $request->validate(
            [
                "title"=>"required|max:191",
                "description"=>"required"
            ]
        );
        $status = Status::where("name", Status::LINE1)->first();
        $ticket = new Ticket();
        $ticket->title = $request->title;
        $ticket->description = $request->description;
        $ticket->status()->associate($status);
        $request->user()->submitted_tickets()->save($ticket);
        return redirect()->route("ticket_index_customer")->with("success", "Your ticket is saved...");
    }
    
    public function index_customer(){
        $this->authorize("create", Ticket::class);
        $tickets = Auth::user()->submitted_tickets()->orderBy("created_at", "DESC")->get();
        return view("ticket.index", ["tickets" => $tickets]);
    }
    
    public function index_helpdesk(){
        $this->authorize("assign", Ticket::class);
        $assigned_tickets = Auth::user()->assigned_tickets()->orderBy("created_at", "DESC")->get();
        if (Auth::user()->role->name == Role::LINE1){
            $unassigned_tickets = Status::where("name", Status::LINE1)->first()->tickets;
        }else{
            $unassigned_tickets = Status::where("name", Status::LINE2)->first()->tickets;
        }
        return view(
            "ticket.index_helpdesk", [
                "assigned_tickets" => $assigned_tickets,
                "unassigned_tickets" => $unassigned_tickets
            ]
        );
    }
    
    public function show($id){
        $ticket = Ticket::findOrFail($id);
        $this->authorize("show", $ticket);
        $var["ticket"] = $ticket;
        if (Auth::user()->can("delegate", $ticket)){
            $var["colleagues"] = Auth::user()->role->users->diff([Auth::user()]);
        }
        return view("ticket.show", $var);
    }
    
    public function close($id){
        $ticket = Ticket::findOrFail($id);
        $this->authorize("close", $ticket);
        $status = Status::where("name", Status::HANDLED)->first();
        $ticket->status()->associate($status);
        $ticket->save();
        return redirect()->back()->with("success", __("Your ticket is closed..."));
    }
    
    public function claim($id){
        $ticket = Ticket::findOrFail($id);
        $this->authorize("claim", $ticket);
        if (Auth::user()->role->name == Role::LINE1){
            $status = Status::where("name", Status::LINE1A)->first();
        }else{
            $status = Status::where("name", Status::LINE2A)->first();
        }
        $ticket->status()->associate($status);
        Auth::user()->assigned_tickets()->attach($id);
        $ticket->save();
        return redirect()->back()->with("success", __("ticket is assigned to you..."));
    }
    
    public function withdraw($id){
        $ticket = Ticket::findOrFail($id);
        $this->authorize("withdraw", $ticket);
        if (Auth::user()->role->name == Role::LINE1){
            $status = Status::where("name", Status::LINE1)->first();
        }else{
            $status = Status::where("name", Status::LINE2)->first();
        }
        $ticket->status()->associate($status);
        Auth::user()->assigned_tickets()->detach($id);
        $ticket->save();
        return redirect()->back()->with("success", __("User has withdrawn..."));
    }
    
    public function escalate($id){
        $ticket = Ticket::findOrFail($id);
        $this->authorize("escalate", $ticket);
        $status = Status::where("name", Status::LINE2)->first();
        $ticket->status()->associate($status);
        $ticket->save();
        return redirect()->back()->with("success", __("Ticket successfully escalated..."));
    }
    
    public function deEscalate($id){
        $ticket = Ticket::findOrFail($id);
        $this->authorize("deEscalate", $ticket);
        $status = Status::where("name", Status::LINE1A)->first();
        $ticket->status()->associate($status);
        Auth::user()->assigned_tickets()->detach($id);
        $ticket->save();
        return redirect()->back()->with("success", __("Ticket successfully de escalated..."));
    }
    
    public function delegate($id, Request $request){
        $ticket = Ticket::findOrFail($id);
        $this->authorize("delegate", $ticket);
    
        $request->validate([
            "delegate_user" => "exists:users,id"
        ]);
        $delegate_user = User::find($request->delegate_user);
        if ($delegate_user->is(Auth::user()) || $delegate_user->isNot(Auth::user()->role)){
            return redirect()->back();
        }
        Auth::user()->assigned_tickets()->detach($id);
        $delegate_user->assigned_tickets()->attach($id);
        return redirect()->back()->with("success", __("Ticket successfully delegated to "+$delegate_user->name+"..."));
    }
}
