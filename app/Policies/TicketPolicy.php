<?php

namespace App\Policies;

use App\User;
use App\Ticket;
use App\Role;
use App\Status;
use Illuminate\Auth\Access\HandlesAuthorization;

class TicketPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function show(User $user, Ticket $ticket){
        return
            $user->submitted_tickets->contains($ticket)
            ||
            $user->role->name == Role::LINE1
            ||
            $user->role->name == Role::LINE2;
    }
    
    public function create(User $user){
        return $user->role->name == Role::KLANT;
    }
    
    public function close(User $user, Ticket $ticket){
        return
            $ticket->isOpen()
            &&
            ($user->submitted_tickets->contains($ticket)
            ||
            $user->assigned_tickets->contains($ticket));
    }
    
    public function assign(User $user){
//        De lijn hieronder zou alle roles behalve admin en klant toegang geven
//        Dus ook nieuw toegevoegde roles.
//        Maar als je een rol wil toevoegen die geen toegang heeft, moet je weer de code induiken, dus dat helpt dan niks
//        return $user->role->name != Role::ADMIN || Role::KLANT;
        return $user->role->name == Role::LINE1 || $user->role->name == Role::LINE2;
    }
    
    public function comment(User $user, Ticket $ticket){
        return
            $ticket->isOpen() && ($user->is($ticket->submitting_user)
                ||
                $user->assigned_tickets->contains($ticket)
                ||
                $user->submitted_tickets->contains($ticket)
            );
    }
    
    public function claim(User $user, Ticket $ticket){
        return
            ($ticket->status->name == Status::LINE1 && $user->role->name == Role::LINE1)
            ||
            ($ticket->status->name == Status::LINE2 && $user->role->name == Role::LINE2);
    }
    
    public function withdraw(User $user, Ticket $ticket){
        return
            $user->assigned_tickets->contains($ticket)
            &&
            (($ticket->status->name == Status::LINE1A && $user->role->name == Role::LINE1)
            ||
            ($ticket->status->name == Status::LINE2A && $user->role->name == Role::LINE2));
    }
    
    public function escalate(User $user, Ticket $ticket){
        return
            $user->assigned_tickets->contains($ticket)
            &&
            ($user->role->name == Role::LINE1 && $ticket->status->name == Status::LINE1A);
    }
    
    public function deEscalate(User $user, Ticket $ticket){
        return
            $user->assigned_tickets->contains($ticket)
            &&
            ($user->role->name == Role::LINE2 && $ticket->status->name == Status::LINE2A);
    }
    
    public function delegate(User $user, Ticket $ticket){
        return
            $ticket->isOpen()
            &&
            $user->assigned_tickets->contains($ticket)
            &&
            (
                ($user->role->name == Role::LINE1 && Role::where("name", Role::LINE1)->first()->users()->count()>1)
                ||
                ($user->role->name == Role::LINE2 && Role::where("name", Role::LINE2)->first()->users()->count()>1)
            )
            &&
            (
                ($user->role->name == Role::LINE1 && $ticket->status->name != Role::LINE1)
                ||
                ($ticket->status->name == Status::LINE2A && $user->role->name == Role::LINE2)
            );
    }
}