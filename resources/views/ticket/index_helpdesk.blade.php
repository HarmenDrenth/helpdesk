@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Assigned Tickets</div>

                    <div class="card-body">
                        @if(session("success"))
                            <div class="alert alert-success">
                                {{ session("success") }}
                            </div>
                        @endif
                        @forelse ($assigned_tickets as $ticket)
                            <div class="card mb-3">
                                <div class="card-header">
                                    {{ $ticket->customer_user->name }}
                                    <em>{{ $ticket->created_at->toFormattedDateString() }}</em>
                                </div>
                                <div class="card-body">
                                    <h5 class="card_title">
                                        @can("show", $ticket)
                                            <a href="{{ route("ticket_show", ["id" => $ticket]) }}">{{ $ticket->title }}</a>
                                        @else
                                            {{ $ticket->title }}
                                        @endcan
                                    </h5>
                                    <p class="card-text">
                                        {!! nl2br(e($ticket->description)) !!}
                                    </p>
                                </div>
                                <div class="card-footer">
                                    {{ $ticket->status->description }}
                                </div>
                            </div>
                        @empty
                            {{ __("No assigned tickets available...") }}
                        @endforelse
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Unassigned tickets</div>

                    <div class="card-body">
                        @if(session("success"))
                            <div class="alert alert-success">
                                {{ session("success") }}
                            </div>
                        @endif
                        @forelse ($unassigned_tickets as $ticket)
                            <div class="card mb-3">
                                <div class="card-header">
                                    {{ $ticket->customer_user->name }}
                                    <em>{{ $ticket->created_at->toFormattedDateString() }}</em>
                                </div>
                                <div class="card-body">
                                    <h5 class="card_title">
                                        {{ $ticket->title }}
                                    </h5>
                                    <p class="card-text">
                                        {!! nl2br(e($ticket->description)) !!}
                                    </p>
                                </div>
                                <div class="card-footer">
                                    {{ $ticket->status->description }}
                                    @can("claim", $ticket)
                                        <form action="{{ route("ticket_claim",["id" => $ticket]) }}" method="post" class="d-inline">
                                            <input type="hidden" name="_method" value="PUT">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="submit" value="claim ticket">
                                        </form>
                                    @endcan
                                </div>
                            </div>
                        @empty
                            {{ __("No unassigned tickets available...") }}
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
