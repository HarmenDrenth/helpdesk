@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Tickets</div>
                    @if($ticket)
                        @can("close", $ticket)
                            <form action="{{ route("ticket_close",["id" => $ticket]) }}" method="post" class="d-inline">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" value="close ticket">
                            </form>
                        @endcan
                        @can("withdraw", $ticket)
                            <form action="{{ route("ticket_withdraw",["id" => $ticket]) }}" method="post" class="d-inline">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" value="withdraw ticket">
                            </form>
                        @endcan
                        @can("escalate", $ticket)
                            <form action="{{ route("ticket_escalate",["id" => $ticket]) }}" method="post" class="d-inline">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" value="escalate ticket">
                            </form>
                        @endcan
                        @can("deEscalate", $ticket)
                            <form action="{{ route("ticket_deescalate",["id" => $ticket]) }}" method="post" class="d-inline">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" value="de escalate ticket">
                            </form>
                        @endcan
                        @can("delegate", $ticket)
                            <button type="button" class="btn btn-primary"
                                    data-toggle="modal" data-target="#modalDelegate">
                                {{ __("Delegate") }}
                            </button>
                        @endcan
                        <div class="card-body">
                            @if(session("success"))
                                <div class="alert alert-success">
                                    {{ session("success") }}
                                </div>
                            @endif

                                <div class="card mb-3">
                                    <div class="card-header">
                                        {{ $ticket->customer_user->name }}
                                        <em>{{ $ticket->created_at->toFormattedDateString() }}</em>
                                    </div>
                                    <div class="card-body">
                                        <h5 class="card_title">
                                            {{ $ticket->title }}
                                        </h5>
                                        <p class="card-text">
                                            {!! nl2br(e($ticket->description)) !!}
                                        </p>
                                    </div>
                                    <div class="card-footer">
                                        {{ $ticket->status->description }}
                                    </div>
                                </div>
                                <div class="card mb-3">
                                    <div class="card-header">{{ __("Comments") }}</div>
                                    <div class="card-body">
                                        @forelse($ticket->comments as $comment)
                                            <div class="card mb-3">
                                                <div class="card-header">
                                                    {{ $comment->user->name }}
                                                    <em>{{ $comment->created_at->toFormattedDateString() }}</em>
                                                </div>
                                                <div>
                                                    <p class="card-text">
                                                        {!! nl2br(e($comment->contents)) !!}
                                                    </p>
                                                </div>
                                            </div>
                                        @empty
                                            {{ __("No comments found.") }}
                                        @endforelse
                                    </div>
                                    @if(session("success"))
                                        <div class="alert-success">
                                            {{ session("success") }}
                                        </div>
                                    @endif
                                    <div class="card-footer" id="commentCreate">
                                        @can("comment", $ticket)
                                            <p>{{ __("Create comment") }}</p>
                                            <form action="{{ route("comment_save",["id" => $ticket]) }}" method="POST" id="form">
                                                @csrf
                                                <input type="hidden" name="_method" value="PUT">
                                                <div class="form-group row">
                                                    <label for="contents" class="col-md-4 col-form-label text-md-right">{{ __('contents') }}</label>
                                                    <div class="col-md-6">
                                                        <textarea id="contents" class="form-control{{ $errors->has('contents') ? ' is-invalid' : '' }}" name="contents">{{old("contents")}}</textarea>
                                                        @if ($errors->has('contents'))
                                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('contents') }}</strong>
                                                </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <div class="col-md-6 offset-md-4">
                                                        <button type="submit" class="btn btn-primary">
                                                            {{ __('submit') }}
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        @endcan
                                    </div>
                                </div>
                            @else
                            {{ __("No ticket available...") }}
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- stap 39 --}}
    @can('delegate', $ticket)
        <div class="modal fade" id="modalDelegate" tabindex="-1"
        role="dialog" aria-labelledby="Delegate ticket" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered"
            role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Delegate ticket</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route("ticket_delegate",["id" => $ticket]) }}" method="POST" id="form">
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group row">
                                <label for="contents" class="col-md-4 col-form-label text-md-right">{{ __('contents') }}</label>
                                <div class="col-md-6">
                                    <select name="delegate_choice" id="delegate_choice">
                                        @foreach($colleagues as $colleague)
                                            <option value="{{ $colleague->id }}">{{ $colleague->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endcan
@endsection