<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [Role::ADMIN, Role::KLANT, Role::LINE1, Role::LINE2];
        foreach ($roles as $role){
            DB::table('roles')->insert([
                'name' => $role,
                "created_at" => now(),
                "updated_at" => now()
            ]);
        }
    }
}
