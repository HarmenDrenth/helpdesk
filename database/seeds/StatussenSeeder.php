<?php

use Illuminate\Database\Seeder;
USE App\Status;

class StatussenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 0;
        $statussen = [
            [Status::LINE1, "Dit is de eerste lijn"],
            [Status::LINE1A,"De eerste lijn is toegewezen"],
            [Status::LINE2, "Dit is de tweede lijn"],
            [Status::LINE2A, "De tweede lijn is toegewezen"],
            [Status::HANDLED, "Afgehandeld"]
        ];
        
        
        foreach ($statussen as $status){
            $i++;
            DB::table("statuses")->insert([
                "name" => $status[0],
                "description" => $status[1],
                "rank" => $i,
                "created_at" => now(),
                "updated_at" => now()
            ]);
        }
    }
}