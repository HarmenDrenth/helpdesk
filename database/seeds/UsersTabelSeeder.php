<?php

use Illuminate\Database\Seeder;
use App\Role;

class UsersTabelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $role_ids = Role::pluck("id","name");
        $users = [
            ["Jordy","jqhaveman@st.noorderpoort.nl", Role::ADMIN],
            ["Jeroen","jb.vanderbrink@noorderpoort.nl", Role::KLANT],
            ["Reygan","ra.leito@noorderpoort.nl", Role::KLANT],
            ["Jona","jjvandermeulen@st.noorderpoort.nl", Role::LINE1],
            ["Jelle", "jwobbes1@st.noorderpoort.nl", Role::LINE1],
            ["Eva", "ekahlmann@st.noorderpoort.nl", Role::LINE2],
            ["Thijs","trfhadderingh@st.noorderpoort.nl",Role::LINE2]
        ];
        foreach ($users as $user){
            DB::table('users')->insert([
                'name' => $user[0],
                'email' => $user[1],
                "role_id" => $role_ids[$user[2]],
                'password' => bcrypt('secret'),
                "created_at" => now(),
                "updated_at" => now()
            ]);
        }
    }
}
