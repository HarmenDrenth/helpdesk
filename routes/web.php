<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get("/", "HomeController@index")->name("");

Route::get('/home', 'HomeController@index')->name('home');

Route::get("/ticket/create", "TicketController@create")->name("ticket_create");

Route::post("/ticket/save", "TicketController@save")->name("ticket_save");

Route::get("/ticket/index_customer", "TicketController@index_customer")->name("ticket_index_customer");

Route::get("ticket/{id}/show", "TicketController@show")->name("ticket_show");

Route::put("/ticket/{id}/update", "TicketController@update")->name("ticket_update");

Route::post("/ticket/{id}/comment/save","CommentController@save")->name("comment_save");

Route::get("/ticket/index_helpdesk", "TicketController@index_helpdesk")->name("ticket_index_helpdesk");

Route::put("/ticket/{id}/close", "TicketController@close")->name("ticket_close");

Route::put("/ticket/{id}/claim", "TicketController@claim")->name("ticket_claim");

Route::put("/ticket/{id}/withdraw", "TicketController@withdraw")->name("ticket_withdraw");

Route::put("/ticket/{id}/escalate", "TicketController@escalate")->name("ticket_escalate");

Route::put("/ticket/{id}/deescalate", "TicketController@deEscalate")->name("ticket_deescalate");

Route::put("/ticket/{id}/delegate", "TicketController@delegate")->name("ticket_delegate");
